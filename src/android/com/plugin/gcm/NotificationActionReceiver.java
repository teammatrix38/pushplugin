package com.plugin.gcm;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.widget.Toast;
import android.app.NotificationManager;

import com.plugin.gcm.NotificationActionService;

/**
 * Created by and on 19/1/16.
 */
public class NotificationActionReceiver extends WakefulBroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
	try{
		Intent serveic = new Intent(context,NotificationActionService.class);
		serveic.putExtra("url",intent.getStringExtra("url"));
		NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		manager.cancel(intent.getStringExtra("tagName"),intent.getIntExtra("nId",0));
		Toast.makeText(context,intent.getIntExtra("nId",0)+"",Toast.LENGTH_LONG).show();
        startWakefulService(context, serveic);
		}catch(Exception e){
			Toast.makeText(context,e.getMessage(),Toast.LENGTH_LONG).show();
		}
    }
}