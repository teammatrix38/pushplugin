package com.plugin.gcm;

import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import java.net.URL;
import java.net.HttpURLConnection;
import java.net.URLConnection;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.BufferedReader;
import android.widget.Toast;

import android.content.Context;

/**
 * Created by and on 19/1/16.
 */
public class NotificationActionService extends IntentService {

    public NotificationActionService() {
        super(NotificationActionService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(final Intent intent) {
		
		Toast.makeText(getApplicationContext() ,intent.getStringExtra("url"),Toast.LENGTH_LONG).show();
		String urls = intent.getStringExtra("url");
		try {
			URL imageUrl = new URL(urls);
			HttpURLConnection conn = (HttpURLConnection)imageUrl.openConnection();
			conn.setConnectTimeout(30000);
			conn.setReadTimeout(30000);
			conn.setInstanceFollowRedirects(true);
			InputStream is=conn.getInputStream();
			BufferedReader nReader = new BufferedReader(new InputStreamReader(is));
			String temp,response="";
			while((temp=nReader.readLine())!=null){
				response+=temp;
			}
		}catch(Exception e){
		} finally{
		}
        NotificationActionReceiver.completeWakefulIntent(intent);
    }
}