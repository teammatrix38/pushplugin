package com.plugin.gcm;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.app.PendingIntent;
import android.graphics.drawable.Icon;
import android.graphics.Color;
import android.content.Context;
import android.content.res.AssetManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.net.HttpURLConnection;
import java.net.URLConnection;
import java.lang.Object;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.*;
import android.webkit.URLUtil;
import android.content.SharedPreferences;

import com.google.android.gcm.GCMBaseIntentService;
import com.plugin.gcm.actionButton;
import com.plugin.gcm.NotificationActionReceiver;

@SuppressLint("NewApi")
public class GCMIntentService extends GCMBaseIntentService {

	private static final String TAG = "GCMIntentService";
	
	public GCMIntentService() {
		super("GCMIntentService");
	}

	@Override
	public void onRegistered(Context context, String regId) {

		Log.v(TAG, "onRegistered: "+ regId);

		JSONObject json;

		try
		{
			json = new JSONObject().put("event", "registered");
			json.put("regid", regId);

			Log.v(TAG, "onRegistered: " + json.toString());

			// Send this JSON data to the JavaScript application above EVENT should be set to the msg type
			// In this case this is the registration ID
			PushPlugin.sendJavascript( json );

		}
		catch( JSONException e)
		{
			// No message to the user is sent, JSON failed
			Log.e(TAG, "onRegistered: JSON exception");
		}
	}

	@Override
	public void onUnregistered(Context context, String regId) {
		Log.d(TAG, "onUnregistered - regId: " + regId);
	}

	@Override
	protected void onMessage(Context context, Intent intent) {
		Log.d(TAG, "onMessage - context: " + context);

		// Extract the payload from the message
		Bundle extras = intent.getExtras();
		if (extras != null)
		{
			// if we are in the foreground, just surface the payload, else post it to the statusbar
            if (PushPlugin.isInForeground()) {
				extras.putBoolean("foreground", true);
                PushPlugin.sendExtras(extras);
			}
			else {
				extras.putBoolean("foreground", false);

                // Send a notification if there is a message
                if (extras.getString("message") != null && extras.getString("message").length() != 0) {
                    createNotification(context, extras);
                }
            }
        }
	}

	public void createNotification(Context context, Bundle extras)
	{
		NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		int defaults = Notification.DEFAULT_ALL;
	try{
		SharedPreferences sharedPref = context.getSharedPreferences("notification",Context.MODE_PRIVATE);
		String appIcon = sharedPref.getString("icon","");
		int appIconColor = sharedPref.getInt("iconColor",0);
		int notId = 0;
		
		try {
			notId = Integer.parseInt(extras.getString("notId"));
		}
		catch(NumberFormatException e) {
			Log.e(TAG, "Number format exception - Error parsing Notification ID: " + e.getMessage());
		}
		catch(Exception e) {
			Log.e(TAG, "Number format exception - Error parsing Notification ID" + e.getMessage());
		}
		if(notId==0){
			int min = 20;
			int max = 999999999;
			Random r = new Random();
			int i1 = r.nextInt(max-min+1)+min;
			notId = i1;
		}
		String appName = getAppName(this);

		Intent notificationIntent = new Intent(this, PushHandlerActivity.class);
		notificationIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
		notificationIntent.putExtra("pushBundle", extras);

		PendingIntent contentIntent = PendingIntent.getActivity(this, notId, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		
		

		if (extras.getString("defaults") != null) {
			try {
				defaults = Integer.parseInt(extras.getString("defaults"));
			} catch (NumberFormatException e) {}
		}
		
		
		Bitmap icons = getBitmapFromAsset(context,appIcon);
		NotificationCompat.Builder mBuilder =
			new NotificationCompat.Builder(context)
				.setVisibility(Notification.VISIBILITY_PUBLIC)
				.setDefaults(defaults)
				.setSmallIcon(context.getApplicationInfo().icon)
				.setLargeIcon(icons)
				.setColor(appIconColor)
				//.setSmallIcon(context.getResources().getIdentifier("iconn", "drawable", context.getPackageName()))
				.setWhen(System.currentTimeMillis())
				.setContentTitle(extras.getString("title"))
				//.setContentTitle(String.valueOf(context.getResources().getIdentifier("iconn", "drawable", context.getPackageName())))
				.setTicker(extras.getString("title"))
				.setContentIntent(contentIntent)
				//.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(banner))
				.setAutoCancel(true)
				.setPriority(NotificationCompat.PRIORITY_MAX);
		String message = extras.getString("message");		
		if(context.getResources().getIdentifier("pushnotification", "drawable", context.getPackageName())>0){
			mBuilder.setSmallIcon(context.getResources().getIdentifier("pushnotification", "drawable", context.getPackageName()));
		}

		
		int setMessage = 0;
		if(extras.getString("banner")!=null){
			setMessage = 2;
		}
		ArrayList<actionButton> buttonList = new ArrayList<actionButton>();
		if(extras.getString("notification_type")!=null)
		{
			String nType = extras.getString("notification_type");
			if(nType.equals("askQ")){
				setMessage=1;
				try{
				JSONArray ButtonArray = new JSONArray(extras.get("askButton").toString());
				for(int i=0;i<ButtonArray.length();i++){
					actionButton tempButton = new actionButton();
					JSONObject objq = (JSONObject) ButtonArray.get(i);
					if(objq.has("buttonText")){
						tempButton.setbuttonText(objq.getString("buttonText"));
					}
					if(objq.has("url")){
						tempButton.setUrl(objq.getString("url"));
					}
					buttonList.add(tempButton);
					//mBuilder.setContentTitle(objq.get("buttonText").toString());
				}
				// JSONObject objq = (JSONObject) ButtonArray.get(0);
				// mBuilder.setContentTitle(objq.get("buttonText").toString());
				}catch(JSONException ex){
					Log.e(TAG, "JSON exception - Error parsing Notification ID: " + ex.getMessage());
				}catch(Exception ex){
					Log.e(TAG, "JSON exception - Error parsing Notification ID: " + ex.getMessage());
				}
			}else if(nType.equals("banner")){
				setMessage=2;
			}
		}
		//mBuilder.setContentTitle();
		if(setMessage==0){
			if (message != null) {
				mBuilder.setContentText(message);
			} else {
				mBuilder.setContentText("<missing message content>");
			}
		}else if(setMessage==1){
			Iterator itr = buttonList.iterator();
			int btnNumber=0;
			while(itr.hasNext()){
				btnNumber++;
				actionButton acb = (actionButton)itr.next();
				Intent abutton = new Intent(context, NotificationActionReceiver.class);
				abutton.putExtra("url",acb.getUrl());
				abutton.putExtra("nId",notId);
				abutton.putExtra("tagName",appName);
				PendingIntent contentIntent1 = PendingIntent.getBroadcast(context, btnNumber, abutton, PendingIntent.FLAG_UPDATE_CURRENT);
				mBuilder.addAction(context.getApplicationInfo().icon,acb.getbuttonText(),contentIntent1);
			}
			//mBuilder.setShowActionsInCompactView(0);
			/*NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
			inboxStyle.addLine(message);*/
			//mBuilder.setStyle(inboxStyle);
			mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
			//mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(extras.toString()));
		}else if(setMessage==2){
			String Url = extras.getString("banner");
			if(URLUtil.isValidUrl(Url)){
				Bitmap banner=null;
				 try {
					banner = BitmapFactory.decodeStream((InputStream) new URL(Url).getContent());
				} catch (IOException e) {
                    e.printStackTrace();
                }
				mBuilder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(banner).setSummaryText(extras.getString("message")));
			}
		}

		String msgcnt = extras.getString("msgcnt");
		if (msgcnt != null) {
			mBuilder.setNumber(Integer.parseInt(msgcnt));
		}
		
		
		
		mNotificationManager.notify((String) appName, notId, mBuilder.build());
		}catch(Exception exe){
			NotificationCompat.Builder mBuilder =
			new NotificationCompat.Builder(context)
				.setVisibility(Notification.VISIBILITY_PUBLIC)
				.setDefaults(defaults)
				.setSmallIcon(context.getApplicationInfo().icon)
				//.setSmallIcon(context.getResources().getIdentifier("iconn", "drawable", context.getPackageName()))
				.setWhen(System.currentTimeMillis())
				.setContentTitle(extras.getString("title"))
				//.setContentTitle(String.valueOf(context.getResources().getIdentifier("iconn", "drawable", context.getPackageName())))
				.setTicker(extras.getString("title"))
				//.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(banner))
				.setAutoCancel(true)
				.setPriority(NotificationCompat.PRIORITY_MAX);
			mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(exe.getMessage()));
			mNotificationManager.notify("Error", 0, mBuilder.build());
		}
	}
	
	private static Bitmap getBitmapFromAsset(Context context,String filePath){
		AssetManager assetManager = context.getAssets();
		InputStream istr;
		Bitmap bitmap = null;
		try {
			istr = assetManager.open(filePath);
			bitmap = BitmapFactory.decodeStream(istr);
		} catch (IOException e) {
			// handle exception
		}

		return bitmap;
	}
	
	private static String getAppName(Context context)
	{
		CharSequence appName = 
				context
					.getPackageManager()
					.getApplicationLabel(context.getApplicationInfo());
		
		return (String)appName;
	}
	
	@Override
	public void onError(Context context, String errorId) {
		Log.e(TAG, "onError - errorId: " + errorId);
	}

}