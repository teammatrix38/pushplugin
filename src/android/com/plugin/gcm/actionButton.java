package com.plugin.gcm;

public class actionButton{
	private String buttonText = "";
	private String url="";
	
	public void setbuttonText(String text){
		buttonText = text;
	}
	
	public void setUrl(String text){
		url = text;
	}
	
	public String getbuttonText(){
		return buttonText;
	}
	
	public String getUrl(){
		return url;
	}
}